import { overflow, clamp, within, outside, valid, assert } from './index'

test('Package statement: Provides ranged overflow for Numbers, for example (minimum 1 and maximum 3 with value 5 would be 2)', () => {
    expect(overflow(1, 3, 5)).toBe(2)
})

describe('When overflow', () => {
    describe('is given a value', () => {
        test('within range', () => {
            expect(overflow(0, 3, 0)).toBe(0)
            expect(overflow(0, 3, 1)).toBe(1)
            expect(overflow(0, 3, 2)).toBe(2)
            expect(overflow(0, 3, 3)).toBe(3)

            expect(overflow(2, 5, 2)).toBe(2)
            expect(overflow(2, 5, 3)).toBe(3)
            expect(overflow(2, 5, 4)).toBe(4)
            expect(overflow(2, 5, 5)).toBe(5)

            expect(overflow(-1, 1, -1)).toBe(-1)
            expect(overflow(-1, 1, 0)).toBe(0)
            expect(overflow(-1, 1, 1)).toBe(1)

            expect(overflow(-13, -10, -10)).toBe(-10)
        })

        test('larger than maximum', () => {
            expect(overflow(0, 3, 4)).toBe(0)
            expect(overflow(0, 3, 5)).toBe(1)
            expect(overflow(0, 3, 6)).toBe(2)
            expect(overflow(0, 3, 7)).toBe(3)
            expect(overflow(0, 3, 8)).toBe(0)

            expect(overflow(10, 13, 14)).toBe(10)
            expect(overflow(10, 13, 15)).toBe(11)
            expect(overflow(10, 13, 16)).toBe(12)
            expect(overflow(10, 13, 17)).toBe(13)
            expect(overflow(10, 13, 18)).toBe(10)

            expect(overflow(-1, 1, 2)).toBe(-1)

            expect(overflow(-13, -10, -9)).toBe(-13)
        })

        test('lower than minimum', () => {
            expect(overflow(0, 3, -1)).toBe(3)
            expect(overflow(0, 3, -2)).toBe(2)
            expect(overflow(0, 3, -3)).toBe(1)
            expect(overflow(0, 3, -4)).toBe(0)
            expect(overflow(0, 3, -5)).toBe(3)

            expect(overflow(10, 13, 9)).toBe(13)
            expect(overflow(10, 13, 8)).toBe(12)
            expect(overflow(10, 13, 7)).toBe(11)
            expect(overflow(10, 13, 6)).toBe(10)
            expect(overflow(10, 13, 5)).toBe(13)

            expect(overflow(-1, 1, -2)).toBe(1)

            expect(overflow(-13, -10, -14)).toBe(-10)
        })
    })

    describe('is given an invalid range', () => {
        test('with value lower than minimum', () => {
            expect(clamp(20, 10, 19)).toBe(20)
        })
        test('with value higher or equal than minimum', () => {
            expect(clamp(20, 10, 20)).toBe(10)
        })
    })
})

describe('When clamp', () => {
    describe('is given a value', () => {
        test('within range', () => {
            expect(clamp(0, 3, 0)).toBe(0)
            expect(clamp(0, 3, 1)).toBe(1)
            expect(clamp(0, 3, 2)).toBe(2)
            expect(clamp(0, 3, 3)).toBe(3)
        })

        test('larger than maximum', () => {
            expect(clamp(0, 3, 4)).toBe(3)
            expect(clamp(10, 13, 14)).toBe(13)
            expect(clamp(-13, -10, -9)).toBe(-10)
            expect(clamp(-1, 1, 2)).toBe(1)
        })
        test('lower than minimum', () => {
            expect(clamp(0, 3, -1)).toBe(0)
            expect(clamp(10, 13, 9)).toBe(10)
            expect(clamp(-13, -10, -14)).toBe(-13)
            expect(clamp(-1, 1, -2)).toBe(-1)
        })
    })
    describe('is given an invalid range', () => {
        test('with value lower than minimum', () => {
            expect(clamp(20, 10, 19)).toBe(20)
        })
        test('with value higher or equal than minimum', () => {
            expect(clamp(20, 10, 20)).toBe(10)
        })
    })
})

describe('When within', () => {
    describe('is given a value', () => {
        test('within range', () => {
            expect(within(0, 3, 0)).toBe(true)
            expect(within(0, 3, 1)).toBe(true)
            expect(within(0, 3, 2)).toBe(true)
            expect(within(0, 3, 3)).toBe(true)
            expect(within(-1, 1, 0)).toBe(true)
        })
        test('larger than maximum', () => {
            expect(within(-1, 1, 2)).toBe(false)
            expect(within(0, 3, 4)).toBe(false)
        })
        test('lower than minimum', () => {
            expect(within(0, 3, -1)).toBe(false)
            expect(within(-1, 1, -2)).toBe(false)
        })
    })
    describe('is given an invalid range', () => {
        test('will always return false', () => {
            expect(within(20, 10, 0)).toBe(false)
            expect(within(20, 10, 30)).toBe(false)
        })
    })
})

describe('When outside', () => {
    describe('is given a value', () => {
        test('within range', () => {
            expect(outside(0, 3, 0)).toBe(false)
            expect(outside(0, 3, 1)).toBe(false)
            expect(outside(0, 3, 2)).toBe(false)
            expect(outside(0, 3, 3)).toBe(false)
            expect(outside(-1, 1, 0)).toBe(false)
        })
        test('larger than maximum', () => {
            expect(outside(-1, 1, 2)).toBe(true)
            expect(outside(0, 3, 4)).toBe(true)
        })
        test('lower than minimum', () => {
            expect(outside(0, 3, -1)).toBe(true)
            expect(outside(-1, 1, -2)).toBe(true)
        })
    })
    describe('is given an invalid range', () => {
        test('will always return false', () => {
            expect(outside(20, 10, 0)).toBe(true)
            expect(outside(20, 10, 30)).toBe(true)
        })
    })
})

describe('When valid', () => {
    test('is given finite numbers as parameters', () => {
        expect(valid(1, 2, 3)).toEqual(true)
    })

    test('is not given finite numbers as parameters', () => {
        expect(() => valid('1', 2, 3)).toThrow(TypeError)
        expect(() => valid('1', 2, 3)).toThrow(
            "The parameter min '1' is not a finite number"
        )
        expect(() => valid(1, '2', 3)).toThrow(
            "The parameter max '2' is not a finite number"
        )
        expect(() => valid(1, 2, '3')).toThrow(
            "The parameter value '3' is not a finite number"
        )
    })

    test('is not given a valid range', () => {
        expect(() => valid(2, 1, 3)).toThrow(RangeError)
        expect(() => valid(2, 1, 3)).toThrow(
            'Minimum value 2 must be lower than maximum 1'
        )
    })
})

describe('When assert', () => {
    test('is given parameters within range', () => {
        expect(assert(1, 3, 2)).toEqual(true)
    })

    test('is not given parameters with in range', () => {
        expect(() => assert(1, 2, 3)).toThrow(RangeError)
        expect(() => assert(1, 2, 3)).toThrow(
            'Value (3) must be within range of 1 and 2'
        )
    })
})
