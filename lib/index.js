'use strict'
Object.defineProperty(exports, '__esModule', { value: true })
exports.overflow = overflow
exports.clamp = clamp
exports.within = within
exports.outside = outside
exports.valid = valid
exports.assert = assert
/**
 * Provides ranged overflow for numbers, for example (min 1 and max 3 with value 5 would be 2).
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {number} A overflowed value within the given range
 */
function overflow(min, max, value) {
    max = max + 1 - min
    return ((((value - min) % max) + max) % max) + min
}

/**
 * Clamps value to either min, max or the original value if it is in range.
 * The equivalent of:
 *   if (min > value) return min
 *   if (max < value) return max
 *   else return value
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {number} A value within the given range
 */
function clamp(min, max, value) {
    return min > value ? min : max < value ? max : value
}

/**
 * Checks if value is within the range of min and max.
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {boolean} A true or false indicating if it is in range or not
 */
function within(min, max, value) {
    return min <= value && value <= max
}

/**
 * Checks if value is outside the range of min and max.
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {boolean} A true or false indicating if it is outside the range
 */
function outside(min, max, value) {
    return min > value || value > max
}

/**
 * Throws a TypeError if any parameter is not a finite number.
 * Throws a RangeError if min is larger than max.
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {boolean} Always true
 */
function valid(min, max, value) {
    if (!Number.isFinite(min))
        throw TypeError(`The parameter min '${min}' is not a finite number`)
    if (!Number.isFinite(max))
        throw TypeError(`The parameter max '${max}' is not a finite number`)
    if (!Number.isFinite(value))
        throw TypeError(`The parameter value '${value}' is not a finite number`)
    if (min > max)
        throw RangeError(
            `Minimum value ${min} must be lower than maximum ${max}`
        )

    return true
}

/**
 * Throws a RangeError if value is not within min and max.
 * @param {number} min Minimum value of the range
 * @param {number} max Maximum value of the range
 * @param {number} value A value inside or outside the range
 * @return {boolean} Always true
 */
function assert(min, max, value) {
    if (within(min, max, value)) return true

    throw RangeError(
        `Value (${value}) must be within range of ${min} and ${max}`
    )
}
