# Ranged Overflow

A **overflow** function that overflows a value inside a range.

But wait, if you install now you'll get a complementary **clamp** and a **within**, I'll even throw in a **outside** too.

WHAT is there even more? Yes, the library comes with a **valid** and an **assert** functions for validation of parameters.

If you haven't installed this library yet, then you should check out the ASCII art and benchmarks below. Don't wait, install now.

## Install

It's that easy.

```bash
npm install ranged-overflow

# For your project

npm install --save ranged-overflow

# For development purposes

npm install --save-dev ranged-overflow

```

Don't have NodeJS yet? [No problem](https://nodejs.org/en/download/), and then [n](https://github.com/tj/n).

## Use

Import it ...

```javascript
import {
    overflow,
    clamp,
    within,
    outside,
    valid,
    assert,
} from 'ranged-overflow'

...
```

Or require it, the old fashion way ...

```javascript
var ranged = require('ranged-overflow')

var overflow = ranged.overflow
var clamp = ranged.clamp
var within = ranged.within
var outside = ranged.outside
var valid = ranged.valid
var assert = ranged.assert

...
```

## Functions

The library consist of functions that calculate a value using a range (min + max), check boundaries, and throw exceptions.

All function take three parameters, a minimum, a maximum and a value. Like so function(min, max, value).

### The overflow

It takes value that can overflow the range (min + max).

Example:

```javascript
overlow(1, 3, 5) === 2
```

This way any value you give overflow will count inside the range like so.

```
Range min 1, max 3:

value             range
        0  - |1  -  2  -  3|  -  4

 0           |            3|
 1           |1            |
 2           |      2      |
 3           |            3|
 4           |1            |
 5           |      2      |
... and on and on and on ...
 120961      |0            |

```

For a real world example you could name an array that wraps around the end, a carousel on web page or even a two dimensional world that wraps around the edges.

### The clamp

It takes value and clamps it to the edges.

Example:

```javascript
clamp(1, 3, 5) === 3
```

This way any value you give clamp will stick to the edges.

```
Range min 1, max 3:

value             range
        0  - |1  -  2  -  3|  -  4

 0           |1            |
 1           |1            |
 2           |      2      |
 3           |            3|
 4           |            3|
 5           |            3|
... and on and on and on ...
 120961      |            3|

```

### The within

Returns true if a value is within the range, else false.

Example:

```javascript
within(1, 3, 5) === false
```

### The outside

Returns true if the value is outside the range, otherwise false

Example:

```javascript
outside(1, 3, 5) === true
```

### The valid

Checks all parameters, min, max and value if they are a finite number, if any of them is not then it throws a TypeError exception.

Also it checks if min is less or equal to max (min <= max), and if not it throws a RangeError exception.

This function always returns a true.

Example:

```javascript
valid(1, 3, 5) === true

valid(1, '3', 5) // throws TypeError
valid(3, 1, 5) // throws RangeError
```

### The assert

Assert checks if value is **within** the range, and if not it will throw an RangeError exception.

This function always returns a true.

Example:

```javascript
assert(1, 3, 3) === true

assert(1, 3, 5) // throws RangeError
```

## Unbiased benchmark results

```
Functions
  overflow x 223,834,804 ops/sec ±0.42% (95 runs sampled)
  clamp x 222,307,625 ops/sec ±0.32% (91 runs sampled)
  within x 223,844,303 ops/sec ±0.42% (93 runs sampled)
  outside x 222,657,700 ops/sec ±0.37% (95 runs sampled)
  assert x 221,045,434 ops/sec ±0.35% (95 runs sampled)
  valid x 220,605,742 ops/sec ±0.32% (93 runs sampled)
Fastest was overflow,within,outside,clamp
```

As you can clearly see from the results, that there is not much difference in speed of all these function compared to each other.

```
Validation + functions
  valid && overflow x 221,914,595 ops/sec ±0.42% (91 runs sampled)
  valid && clamp x 221,793,067 ops/sec ±0.33% (95 runs sampled)
  valid && assert x 200,725,337 ops/sec ±6.44% (86 runs sampled)
Fastest was valid && clamp,valid && assert
```

And there is no real drop in speed if you validate the input with **valid** or not.

## Validating your input

There is no default way to validate input, choice to do so is yours to make.

Just remember to try .. catch or .catch() even.

```javascript
// you could do this ..

valid(min, max, value) && overflow(min, max, value)

// or you could do that ..

if (valid(min, max, value)) {
    overflow(min, max, value)
}

// or you could do this ..

let spill = (min, max, value) =>
    valid(min, max, value) && overflow(min, max, value)

spill(min, max, value)
```

Or any other way is just fine.

```
Fastest validation method
  valid && overflow x 220,400,317 ops/sec ±0.23% (95 runs sampled)
  if valid then overflow x 220,930,642 ops/sec ±0.27% (94 runs sampled)
  spill x 202,382,744 ops/sec ±0.08% (99 runs sampled)
Fastest was if valid then overflow,valid && overflow
```

They are all very similar in speed, but spill seems to have a small penalty for some reason.

## License

[MIT](https://mit-license.org/)
