import Benchmark from 'benchmark'
import { overflow, clamp, within, outside, assert, valid } from './index.mjs'

// eslint-disable-next-line no-undef
console.log('Functions')
new Benchmark.Suite()
    .add('overflow', function () {
        overflow(-1, 1, 0)
    })
    .add('clamp', function () {
        clamp(-1, 1, 0)
    })
    .add('within', function () {
        within(-1, 1, 0)
    })
    .add('outside', function () {
        outside(-1, 1, 0)
    })
    .add('assert', function () {
        assert(-1, 1, 0)
    })
    .add('valid', function () {
        valid(-1, 1, 0)
    })
    .on('cycle', function (event) {
        // eslint-disable-next-line no-undef
        console.log(`  ${String(event.target)}`)
    })
    .on('complete', function () {
        // eslint-disable-next-line no-undef
        console.log(`Fastest was ${this.filter('fastest').map('name')}\n`)
    })
    .run()

// eslint-disable-next-line no-undef
console.log('Validation + functions')
new Benchmark.Suite()
    .add('valid && overflow', function () {
        valid(-1, 1, 0) && overflow(-1, 1, 0)
    })
    .add('valid && clamp', function () {
        valid(-1, 1, 0) && clamp(-1, 1, 0)
    })
    .add('valid && assert', function () {
        valid(-1, 1, 0) && assert(-1, 1, 0)
    })

    .on('cycle', function (event) {
        // eslint-disable-next-line no-undef
        console.log(`  ${String(event.target)}`)
    })
    .on('complete', function () {
        // eslint-disable-next-line no-undef
        console.log(`Fastest was ${this.filter('fastest').map('name')}\n`)
    })
    .run()

let spill = (min, max, value) =>
    valid(min, max, value) && overflow(min, max, value)
// eslint-disable-next-line no-undef
console.log('Fastest validation method')
new Benchmark.Suite()
    .add('valid && overflow', function () {
        valid(-1, 1, 0) && overflow(-1, 1, 0)
    })
    .add('if valid then overflow', function () {
        if (valid(-1, 1, 0)) {
            overflow(-1, 1, 0)
        }
    })
    .add('spill', function () {
        spill(-1, 1, 0)
    })

    .on('cycle', function (event) {
        // eslint-disable-next-line no-undef
        console.log(`  ${String(event.target)}`)
    })
    .on('complete', function () {
        // eslint-disable-next-line no-undef
        console.log(`Fastest was ${this.filter('fastest').map('name')}`)
    })
    .run()
